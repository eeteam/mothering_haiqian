
// test Pattern and Matcher

import java.util.regex.*;

public class TestPattern
{
    public static void main(String[] args)
    {
	String input = args[0];
	String pattern = args[1];
	//String pattern1 = args[2];

	Pattern p = Pattern.compile(pattern);
	Matcher m = p.matcher(input);
	//m.reset();
	if (m.find())
	{
	    System.out.println("group number is " + m.groupCount());
	    for (int i = 1; i <= m.groupCount(); i++)
		System.out.println(m.group(i));	    
	}

	//System.out.println(m.end(1));
	//int l = m.end(1);
	
	// p = Pattern.compile(pattern1);
	// m.usePattern(p);
	// if (m.find(l))
	// {
	//     System.out.println(m.group(1));
	//     System.out.println(m.group(2));
	// }

	//String str = "abcdefg";
	//System.out.println(str.substring(1,2));
    }
}