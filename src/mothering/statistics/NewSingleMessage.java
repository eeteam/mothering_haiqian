
/**
 * store necessary information of one single message
 */

package mothering.statistics;

import java.util.regex.*;
import java.util.*;
import java.io.*;

class NewSingleMessage
{
	private String postIdLine, timeLine, threadIdLine, titleLine;
	private String userIdLine, metaDataLine, textLine, quoteLine;
	int postId = 0;
	int threadId = 0;
	int userId = 0;
	int messageId = 0;
	int threadLength = 0;
	String date = null;
	String time = null;
	boolean isInitiator = false;
	String author = null;
	String replyTo = null; // reply to which user
	ArrayList<String> threadAuthors = null;
	ArrayList<String> threadTexts = null;
	ArrayList<Double> threadScores = null;
	double replyToScoreSign;
	
	NewSingleMessage(String postIdLine, String timeLine, String threadIdLine, 
					   String titleLine, String userIdLine, String metaDataLine, 
					   String textLine, String quoteLine,
					   String threadInitiator, ArrayList<String> threadAuthors,
					   ArrayList<String> threadTexts, ArrayList<Double> threadScores)
		throws NoSuchFieldException
	{
		this.postIdLine = postIdLine;
		this.timeLine = timeLine;
		this.threadIdLine = threadIdLine;
		this.titleLine = titleLine;
		this.userIdLine = userIdLine;
		this.metaDataLine = metaDataLine;
		this.textLine = textLine;
		this.quoteLine = quoteLine;
		this.replyTo = threadInitiator;
		this.threadAuthors = threadAuthors;
		this.threadTexts = threadTexts;
		this.threadScores = threadScores;
		
		try
		{
			parsePostIdLine();
			parseTimeLine();
			parseThreadIdLine();
			// parseTitleLine();
			parseUserIdLine();
			parseMetaDataLine();
			// parseTextLine();
			parseQuoteLine();
		}
		catch (NoSuchFieldException nfe)
		{
			throw nfe;
		}
	}
	
	private void parsePostIdLine() throws NoSuchFieldException
	{
		Pattern p = Pattern.compile("^PostId:(\\d+)");
		Matcher m = p.matcher(postIdLine);
		
		if (m.find())
		{
			postId = Integer.parseInt(m.group(1));
		}
		else
		{
			throw new NoSuchFieldException("cannot find post id in " + postIdLine);
		}
	}
	
	private void parseTimeLine() throws NoSuchFieldException
	{
		Pattern p = Pattern.compile("^Time:(\\d+/\\d+/\\d+)\\s+at\\s+(\\d+:\\d+[ap]m)");
		Matcher m = p.matcher(timeLine);
		
		if (m.find())
		{
			date = m.group(1);
			time = m.group(2);
		}
		else
		{
			throw new NoSuchFieldException("cannot find date/time in " + timeLine);
		}
	}
	
	private void parseThreadIdLine() throws NoSuchFieldException
	{
		Pattern p = Pattern.compile("^ThreadId:(\\d+)");
		Matcher m = p.matcher(threadIdLine);
		
		if (m.find())
		{
			threadId = Integer.parseInt(m.group(1));
		}
		else
		{
			throw new NoSuchFieldException("cannot find thread id in " + threadIdLine);
		}
	}
	
	private void parseTitleLine() throws NoSuchFieldException
	{
		//
	}
	
	private void parseUserIdLine() throws NoSuchFieldException
	{
		Pattern p = Pattern.compile("^UserId:(\\d+)");
		Matcher m = p.matcher(userIdLine);
		
		if (m.find())
		{
			userId = Integer.parseInt(m.group(1));
		}
		else
		{
			throw new NoSuchFieldException("cannot find user id in " + userIdLine);
		}
	}
	
	private void parseMetaDataLine() throws NoSuchFieldException
	{
		Pattern p = Pattern.compile("^MetaData:post\\s+#(\\d+)\\s+of\\s+(\\d+)");
		Matcher m = p.matcher(metaDataLine);
		int previousMatchEnd = 0;
		int tmpEnd = 0;
		
		// message ID and thread length
		if (m.find(previousMatchEnd)) 
		{
			messageId = Integer.parseInt(m.group(1));
			threadLength = Integer.parseInt(m.group(2));
			previousMatchEnd = m.end(2);
		} 
		else
		{
			throw new NoSuchFieldException("cannot find messageId/thread length in " + metaDataLine);
		}
		
		// is thread starter?
		p = Pattern.compile("\\s+Thread\\s+Starter\\s+;");
		m.usePattern(p);
		if (m.find(previousMatchEnd)) 
		{
			isInitiator = true;
			replyTo = null;
			previousMatchEnd = m.end();
		} 
		else
		{
			isInitiator = false;
		}
		
		// author name
		p = Pattern.compile("\\s+(\\S+(\\s\\S+)*)\\s+Trader\\sFeedback:");
		m.usePattern(p);
		if (m.find(previousMatchEnd))
		{
			author = m.group(1);
			previousMatchEnd = m.end(1);
		}
		else
		{
			throw new NoSuchFieldException("cannot find author in " + metaDataLine);
		}
	}
	
	private void parseTextLine() throws NoSuchFieldException
	{
		
	}
	
	private void parseQuoteLine() throws NoSuchFieldException
	{
		boolean findMatch = false;
		for (int i = 0; i < threadAuthors.size(); i++)
		{
			String previousAuthor = threadAuthors.get(i);
			double previousScore = threadScores.get(i);
			Pattern p = Pattern.compile("Quote:\\s+Originally Posted by " + Pattern.quote(previousAuthor) + "\\s+");
			Matcher m = p.matcher(quoteLine);
		
			if (m.find())
			{
				replyTo = previousAuthor;
				replyToScoreSign = Math.signum(previousScore);
				findMatch = true;
				break;
			}
		}
		
		if (!findMatch)
		{
			for (int i = 0; i < threadTexts.size(); i++)
			{
				String previousText = threadTexts.get(i);
				String previousAuthor = threadAuthors.get(i);
				double previousScore = threadScores.get(i);
				String ss = null;
				
				Pattern p = Pattern.compile("(.{20})$");
				Matcher m = p.matcher(quoteLine);
				
				if (m.find()) ss = m.group(1);
				
				if (ss != null && previousText.indexOf(ss) != -1)
				{
					replyTo = previousAuthor;
					replyToScoreSign = Math.signum(previousScore);
					findMatch = true;
					break;
				}
;			}
		}
		
		if (!findMatch)
		{
			if (threadAuthors.size() > 0) 
			{
				replyTo = threadAuthors.get(0);
				replyToScoreSign = Math.signum(threadScores.get(0));
			}
			else
			{
				replyTo = null;
				replyToScoreSign = 0;
			}
		}
	}
	
	
	
	public static void main(String[] args)
	{
		//Scanner sc = ReadWrite.fileScanner(args[0]);
		BufferedReader br = ReadWrite.fileBufferedReader(args[0]);
		BufferedReader br1 = ReadWrite.fileBufferedReader(args[1]); // sentiment
		BufferedWriter bw = ReadWrite.outputWriter(args[2]);
		String threadInitiator = null;
		String name;
		//String messageIdLine = sc.nextLine();
		//String titleLine = sc.nextLine();
		//String metaDataLine = sc.nextLine();
		//String textLine; // = sc.nextLine();
		//boolean messageCorrect = false;
		ArrayList<String> threadAuthors = new ArrayList<String>();
		ArrayList<String> threadTexts = new ArrayList<String>();
		ArrayList<Double> threadScores = new ArrayList<Double>();
		boolean eof = false;
		
		final double threshold = 0.2;
		
		while (!eof)
		{
			try
			{
				String postIdLine = br.readLine();
				if (postIdLine == null) break;
				String timeLine = br.readLine();
				String threadIdLine = br.readLine();
				String titleLine = br.readLine();
				String userIdLine = br.readLine();
				String metaDataLine = br.readLine();
				String textLine = br.readLine();
				String quoteLine = br.readLine();
				
				br1.readLine();
				String posScoreLine = br1.readLine();
				String negScoreLine = br1.readLine();
				double posScore = 0, negScore = 0;
				br1.readLine();
				
				Pattern p = Pattern.compile("^PosScore:(.*)$");
				Matcher m = p.matcher(posScoreLine);
				if (m.find()) posScore = Double.parseDouble(m.group(1));
				
				p = Pattern.compile("^NegScore:(.*)$");
				m = p.matcher(negScoreLine);
				if (m.find()) negScore = Double.parseDouble(m.group(1));
				double score = posScore + negScore;
				
				try
				{
					try
					{
						NewSingleMessage message = new NewSingleMessage(
								postIdLine, timeLine, threadIdLine, titleLine, userIdLine, 
								metaDataLine, textLine, quoteLine, threadInitiator, 
								threadAuthors, threadTexts, threadScores);
						if (message.isInitiator) {
							threadInitiator = message.author;
						}
							
						if (message.messageId == 1) 
						{
							while (!threadAuthors.isEmpty())
							{
								threadAuthors.remove(threadAuthors.size()-1);
								threadTexts.remove(threadTexts.size()-1);
								threadScores.remove(threadScores.size()-1);
							}
							message.replyTo = null;
						}
						threadAuthors.add(message.author);
						threadTexts.add(textLine);
						threadScores.add(score);
							
						try
						{
							if (message.replyTo == null) name = "-1&*";
							else name = message.replyTo;
							bw.write(message.threadId + " " + message.messageId + " " + 
									message.author + " " + message.date + " " + message.time + " "
									+ name + " " + Math.signum(score)*message.replyToScoreSign 
									+ "\n");
						} 
						catch (IOException ie)
						{
							System.out.println("error -- " + ie.toString());
						}
					}
					catch (StringIndexOutOfBoundsException siobe)
					{
						System.out.println ("error -- " + siobe.toString());
					}
				}
				catch (NoSuchFieldException nfe)
				{
					System.out.println("error -- " + nfe.toString());
					//System.exit(-1);
					continue;
				}
			}
			catch (IOException ie)
			{
				System.out.println("Error -- " + ie.toString());
				System.exit(-1);
			}
		}
		
		try
		{
			bw.close();
		} 
		catch (IOException ie)
		{
			System.out.println("error -- " + ie.toString());
		}
	}
}