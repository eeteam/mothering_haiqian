
/**
 * create edgelist from the output file of SingleMessage
 * 
 * edge start from replyTo user to user, information propagation
 */

package mothering.statistics;

import java.util.*;
import java.util.regex.*;
import java.io.*;

class EdgeList
{
	private class ActiveTime
	{
		long first;
		long last;
		
		ActiveTime(long first, long last)
		{
			this.first = first;
			this.last = last;
		}
	}
	
	private HashMap<Edge,Double> edges = new HashMap<Edge,Double>();
	private HashMap<Edge,Integer> edgePosScoreCount = new HashMap<Edge,Integer>();
	private HashMap<Edge,Integer> edgeCount = new HashMap<Edge,Integer>();
	private HashMap<Edge,StringBuilder> edgeTime = new HashMap<Edge,StringBuilder>();
	private HashMap<String,Integer> userId = new HashMap<String,Integer>();
	private HashMap<Integer,ActiveTime> userTime = new HashMap<Integer,ActiveTime>();
	private HashMap<String,String> origUserId = new HashMap<String,String>();
	private int idCount = 0;
	
	EdgeList(String sourceFile, String origUserFile)
	{
		Scanner sc = ReadWrite.fileScanner(sourceFile);
		Scanner sco = ReadWrite.fileScanner(origUserFile);
		
		while (sco.hasNextLine())
		{
			String userIdLine = sco.nextLine();
			String userNameLine = sco.nextLine();
			String id = null, name = null;
			sco.nextLine(); sco.nextLine(); sco.nextLine();
			
			Pattern p = Pattern.compile("^UserId:(\\d+)$");
			Matcher m = p.matcher(userIdLine);
			if (m.find()) id = m.group(1);
			
			Pattern p1 = Pattern.compile("^UserName:(.*)$");
			Matcher m1 = p1.matcher(userNameLine);
			if (m1.find()) name = m1.group(1);
			
			if (!origUserId.containsKey(name))
				origUserId.put(name.toLowerCase(), id);
		}
		
		while (sc.hasNextLine())
		{
			Pattern p = Pattern.compile("^(\\d+)\\s(\\d+)\\s(\\S+[\\s\\S+]*)\\s(\\d+/\\d+/\\d+)\\s(\\d+:\\d+[ap]m)\\s(.+)\\s([-\\d\\.]+)$");
			String line = sc.nextLine();
			Matcher m = p.matcher(line);
			
			if (m.find())
			{
				int threadId = Integer.parseInt(m.group(1));
				int messageId = Integer.parseInt(m.group(2));
				String fromUser = m.group(6).toLowerCase(); // from the initiator
				String date = m.group(4);
				String time = m.group(5);
				String toUser = m.group(3).toLowerCase(); // to repliers
				//double posScore = Double.parseDouble(m.group(7));
				//double negScore = Double.parseDouble(m.group(8));
				double score = Double.parseDouble(m.group(7));

				if (!userId.containsKey(toUser)) userId.put(toUser, idCount++);
				if (!fromUser.equals("-1&*") && !userId.containsKey(fromUser)) 
					userId.put(fromUser, idCount++);
				
				if (fromUser.equals("-1&*")) 
				{
					if (messageId == 1) 
						continue;
					else
						fromUser = toUser;
				}		
				
				Edge currentEdge = new Edge(userId.get(fromUser), userId.get(toUser));
				if (!edges.containsKey(currentEdge)) 
				{
					// edges.put(currentEdge, 1);
					edges.put(currentEdge, score);
				} 
				else 
				{
					double currentWeight = edges.get(currentEdge);
					edges.remove(currentEdge);
					// edges.put(currentEdge, currentWeight+1);
					edges.put(currentEdge, currentWeight+score);
				}
				
				if (!edgePosScoreCount.containsKey(currentEdge))
				{
					if (score > 0)
						edgePosScoreCount.put(currentEdge, 1);
					else
						edgePosScoreCount.put(currentEdge, 0);
				}
				else
				{
					if (score > 0)
						edgePosScoreCount.put(currentEdge, edgePosScoreCount.get(currentEdge)+1);
				}
				
				if (!edgeCount.containsKey(currentEdge))
				{
					edgeCount.put(currentEdge, 1);
				}
				else
				{
					edgeCount.put(currentEdge, edgeCount.get(currentEdge)+1);
				}
				
				addEdgeTime(currentEdge, date, time);
			}
			else
			{
				//throw new NoSuchFieldException("one line has no match: " + line);
				System.out.println("one line has no match: " + line);
			}
		}
	}
	
	/**
	 * record the edge time
	 * @param currentEdge
	 * @param date
	 * @param time
	 */
	private void addEdgeTime(Edge currentEdge, String date, String time)
	{
		Pattern pDate = Pattern.compile("(\\d+)/(\\d+)/(\\d+)");
		Matcher mDate = pDate.matcher(date);
		int month, day, year;
		
		Pattern pTime = Pattern.compile("(\\d+):(\\d+)([ap])m");
		Matcher mTime = pTime.matcher(time);
		int hour, minute;
		
		if (mDate.find() && mTime.find())
		{
			month = Integer.parseInt(mDate.group(1));
			day = Integer.parseInt(mDate.group(2));
			year = Integer.parseInt("20" + mDate.group(3));
			
			hour = Integer.parseInt(mTime.group(1));
			minute = Integer.parseInt(mTime.group(2));
			
			char timeRange = mTime.group(3).charAt(0);
			if (timeRange == 'p' && hour != 12) hour += 12;
			
			Calendar cal = new GregorianCalendar(year, month, day, hour, minute);
			long utcTime = cal.getTimeInMillis();
			String utcTimeStr = Long.toString(utcTime);
			
			if (edgeTime.containsKey(currentEdge))
			{
				edgeTime.get(currentEdge).append(";" + utcTimeStr);
			}
			else
			{
				edgeTime.put(currentEdge, new StringBuilder(utcTimeStr));
			}
			
			if (userTime.containsKey(currentEdge.toId))
			{
				if (utcTime < userTime.get(currentEdge.toId).first)
					userTime.put(currentEdge.toId, new ActiveTime(utcTime, userTime.get(currentEdge.toId).last));
				if (utcTime > userTime.get(currentEdge.toId).last)
					userTime.put(currentEdge.toId, new ActiveTime(userTime.get(currentEdge.toId).first, utcTime));
			}
			else
			{
				userTime.put(currentEdge.toId, new ActiveTime(utcTime, utcTime));
			}
		}
	}
	
	void outputEdgeList(String fileName, String userIdFile)
	{
		BufferedWriter bw = ReadWrite.outputWriter(fileName);
		BufferedWriter bwu = ReadWrite.outputWriter(userIdFile);
		
		List<Edge> sortedEdges = new ArrayList<Edge>(edges.keySet());
		Collections.sort(sortedEdges);
		
		try
		{
			for (Edge e : sortedEdges)
			{
				bw.write(e.fromId + " " + e.toId + " " + edges.get(e) + 
						" " + edgeCount.get(e) + " " + edgePosScoreCount.get(e) + " "+ edgeTime.get(e) + "\n");
			}
			bw.close();
			
			for (String user : userId.keySet())
			{
				String origId = origUserId.get(user);
				if (origId == null) origId = "-1"; 
				
				int uid = userId.get(user);
				ActiveTime at = userTime.get(uid);
				long period = 0;
				if (at != null) period = at.last - at.first;
				bwu.write(user + "%" + origId + "%" + period + "% " + uid + "\n");
			}
			bwu.close();
		}
		catch (IOException ie)
		{
			System.out.println("Error -- " + ie.toString());
			System.exit(-1);
		}
	}
	
	public static void main(String[] args)
	{
		EdgeList el = new EdgeList(args[0], args[3]);
		el.outputEdgeList(args[1], args[2]);
	}
}
