
/**
 * collect all message time stamp and turn them from local times into UTC times
 */

package mothering.statistics;

import java.io.*;
import java.util.*;

class TimeStamps
{
	public static void main(String[] args)
	{
		Scanner sc = ReadWrite.fileScanner(args[0]); // read in file
		BufferedWriter bw = ReadWrite.outputWriter(args[1]); // output file
		
		try
		{
			int countBadFormat = 0;
			
			while (sc.hasNextLine())
			{
				StringTokenizer st = new StringTokenizer(sc.nextLine());
				String date, time;
				if (!st.hasMoreTokens()) continue;
				String tmp = st.nextToken();
				//System.out.println(tmp);
				if (tmp.equals("MetaData:post"))
				{
					try
					{
						st.nextToken(); st.nextToken(); st.nextToken(); 
						date = st.nextToken();
						st.nextToken(); 
						time = st.nextToken();
					}
					catch (NoSuchElementException nsee)
					{
						continue;
					}
					
					//System.out.println("date = " + date);
					StringTokenizer dateSt = new StringTokenizer(date, "/");
					StringTokenizer timeSt = new StringTokenizer(time, ":");
					
					try
					{
						int month = Integer.parseInt(dateSt.nextToken());
						int day = Integer.parseInt(dateSt.nextToken());
						int year = Integer.parseInt("20" + dateSt.nextToken());
						int hour = Integer.parseInt(timeSt.nextToken());
						
						String minuteM = timeSt.nextToken();
						int ml = minuteM.length();
						int minute = Integer.parseInt(minuteM.substring(0, ml-2));
						if (minuteM.charAt(ml-2) == 'p')
						{
							if (hour != 12) hour += 12;
						} 
						else if (minuteM.charAt(ml-2) != 'a')
						{
							System.out.println("Error -- ?");
							System.exit(-1);
						}
						
						Calendar cal = new GregorianCalendar(year, month, day, hour, minute);
						bw.write(year + "-" + month + "-" + day + "-" + hour + "-" + minute + " ");
						bw.write(cal.getTimeInMillis() + "\n");
					}
					catch (NumberFormatException nfe)
					{
						countBadFormat++;
					}
				}
			}
			
			System.out.println("Bad format line number is " + countBadFormat);
			
			bw.close();
			sc.close();
		}
		catch (IOException ie)
		{
			System.out.println("Error -- " + ie.toString());
			System.exit(-1);
		}
	}
}