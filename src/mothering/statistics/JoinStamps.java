
/**
 * measure the joined time stamps
 * and convert them into UTC time for plotting histograms
 * 
 */

package mothering.statistics;

import java.io.*;
import java.util.*; 

class JoinStamps
{
	public static void main(String[] args)
	{
		Scanner sc = ReadWrite.fileScanner(args[0]); // read in file
		BufferedWriter bw = ReadWrite.outputWriter(args[1]); // output file
		
		try
		{
			int countBadFormat = 0;
			
			while (sc.hasNextLine())
			{
				Scanner t = new Scanner(sc.nextLine());
				if (!t.hasNext("MetaData:post")) continue;
				String date, time, author, joinTime;
				try
				{
					t.next(); t.next(); t.next(); t.next(); 
					date = t.next(); 
					t.next();
					time = t.next();
					author = t.next();
					if (author.equals("Thread") && t.hasNext("Starter")) {
						t.next(); t.next();
						author = t.next();	
					}
					while (!t.hasNext("Trader")) 
					{
						author = author + t.next();
					}
					
					while (!t.hasNext("Joined")) t.next();
					if (!t.next().equals("Joined")) continue;
					joinTime = t.next();
					
					Scanner joinTimeSc = new Scanner(joinTime).useDelimiter("/");
					try
					{
						int month = joinTimeSc.nextInt();
						int year = joinTimeSc.nextInt();
						
						Calendar cal = new GregorianCalendar(year, month, 15, 12, 0);
						bw.write(author + " ");
						bw.write(year + "-" + month + " ");
						bw.write(cal.getTimeInMillis() + "\n");
					}
					catch (NumberFormatException nfe)
					{
						countBadFormat++;
					}
				}
				catch (NoSuchElementException nsee)
				{
					continue;
				}
			}
			
			System.out.println("Bad format line number is " + countBadFormat);
			
			bw.close();
			sc.close();
		}
		catch (IOException ie)
		{
			System.out.println("Error -- " + ie.toString());
			System.exit(-1);
		}
	}
}

