
package mothering.statistics;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class ReadWrite
{
	// --------------------------------------------------------------------------
	/**
	 * Create a scanner for the file
	 * @param filename
	 * @return file scanner
	 */
	public static Scanner fileScanner(String fileName)
	{
		try
		{
			FileInputStream fin = new FileInputStream(fileName);
			Scanner sc = new Scanner(fin);
			return sc;
		}
		catch (FileNotFoundException fne)
		{
			System.out.println("Error -- " + fne.toString());
			System.exit(-1);
			return null;
		}
	}
	
	public static BufferedReader fileBufferedReader(String fileName)
	{
		try
		{
			FileReader fr = new FileReader(fileName);
			BufferedReader br = new BufferedReader(fr);
			return br;
		}
		catch (Exception e)
		{
			System.out.println("error -- " + e.toString());
			System.exit(-1);
			return null;
		}
	}
	// --------------------------------------------------------------------------
	/**
	 * Create a BUfferedWriter for the output
	 * @param fileName
	 * @return output stream
	 */
	public static BufferedWriter outputWriter(String fileName)
	{
		try
		{
			FileWriter fw = new FileWriter(fileName);
			BufferedWriter bw = new BufferedWriter(fw);
			return bw;
		}
		catch (IOException ie)
		{
			System.out.println("Error -- " + ie.toString());
			System.exit(-1);
			return null;
		}
	}
	
}