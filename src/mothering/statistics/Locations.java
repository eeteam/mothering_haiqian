
/**
 * read clean locations
 */

package mothering.statistics;

import java.io.*;
import java.util.*;
import java.util.regex.*;

class Locations
{
	public static void main(String[] args)
	{
		String fileName = args[0];
		HashMap<String,Integer> stateCount = new HashMap<String,Integer>();
		
		try
		{
			FileReader fr = new FileReader(fileName);
			BufferedReader br = new BufferedReader(fr);
			boolean eof = false;
			int emptyCount = 0;
			
			while (!eof)
			{
				String head = br.readLine(); 
				if (head == null) break;
				br.readLine(); br.readLine(); br.readLine();
				String location = br.readLine(); ;
				
				Pattern p0 = Pattern.compile("^Location:\\s*$");
				Matcher m0 = p0.matcher(location);
				if (m0.find())
				{
					emptyCount++;
				}
				
				Pattern p = Pattern.compile(",\\s([a-zA-Z][a-zA-Z])\\s*$");
				Matcher m = p.matcher(location);
				
				if (m.find())
				{
					String state = m.group(1);
					if (stateCount.containsKey(state))
					{
						stateCount.put(state, stateCount.get(state) + 1);
					}
					else
					{
						stateCount.put(state, 1);
					}
				}
			}
			
			br.close();
			
			FileWriter fw = new FileWriter(args[1]);
			BufferedWriter bw = new BufferedWriter(fw);
			int totalCount = 0;
			for (String state : stateCount.keySet())
			{
				int count = stateCount.get(state);
				bw.write(state + " " + count + "\n");
				totalCount += count;
			}
			bw.close();
			System.out.println("Empty counts " + emptyCount + "; Clean counts " + totalCount);
		}
		catch (FileNotFoundException fnfe)
		{
			System.out.println("Error -- " + fnfe.toString());
			System.exit(-1);
		}
		catch (IOException ie)
		{
			System.out.println("Error -- " + ie.toString());
			System.exit(-1);
		}	
	}
}