
/**
 * store necessary information of one single message
 */

package mothering.statistics;

import java.util.regex.*;
import java.util.*;
import java.io.*;

class SingleMessage
{
	private String messageIdLine, titleLine, metaDataLine, textLine;
	int threadId = 0;
	int messageNumberOfThread = 0;
	int messageId = 0;
	String date = null;
	String time = null;
	boolean isInitiator = false;
	String author = null;
	String replyTo = null; // reply to which user
	ArrayList<String> threadAuthors = null;
	
	SingleMessage(String messageIdLine, String titleLine, 
					String metaDataLine, String textLine,
					String threadInitiator, ArrayList<String> threadAuthors)
		throws NoSuchFieldException
	{
		this.messageIdLine = messageIdLine;
		this.titleLine = titleLine;
		this.metaDataLine = metaDataLine;
		this.textLine = textLine;
		this.replyTo = threadInitiator;
		this.threadAuthors = threadAuthors;
		
		try
		{
			parseMessageIdLine();
			// parseTitleLine();
			parseMetaDataLine();
			parseTextLine();
		}
		catch (NoSuchFieldException nfe)
		{
			throw nfe;
		}
	}
	
	private void parseMessageIdLine() throws NoSuchFieldException
	{
		Pattern p = Pattern.compile(":(\\d+)_(\\d+)");
		Matcher m = p.matcher(messageIdLine);
		if (m.find()) 
		{
			threadId = Integer.parseInt(m.group(1));
			messageId = Integer.parseInt(m.group(2));
		}
		else
		{
			throw new NoSuchFieldException("cannot find thread id or message id in " + messageIdLine);
		}
	}
	
	private void parseMetaDataLine() throws NoSuchFieldException
	{
		Pattern p = Pattern.compile("^MetaData:post\\s+#\\d+\\s+of\\s+(\\d+)");
		Matcher m = p.matcher(metaDataLine);
		int previousMatchEnd = 0;
		int tmpEnd = 0;
		//
		if (m.find(previousMatchEnd)) 
		{
			messageNumberOfThread = Integer.parseInt(m.group(1));
			previousMatchEnd = m.end(1);
		} 
		else
		{
			throw new NoSuchFieldException("cannot find messageNumberOfThread in " + metaDataLine);
		}
		//
		p = Pattern.compile("(\\d+/\\d+/\\d+)\\s+at\\s+(\\d+:\\d+[ap]m)");
		m.usePattern(p);
		if (m.find(previousMatchEnd)) 
		{
			date = m.group(1);
			time = m.group(2);
			previousMatchEnd = m.end(1);
			tmpEnd = m.end(2);
		} 
		else
		{
			throw new NoSuchFieldException("cannot find date or time in " + metaDataLine);
		}
		//
		p = Pattern.compile("at\\s+\\d+:\\d+[ap]m\\s+Thread\\s+Starter\\s+&nbsp;");
		m.usePattern(p);
		if (m.find(previousMatchEnd)) 
		{
			isInitiator = true;
			replyTo = null;
			previousMatchEnd = m.end();
		} 
		else
		{
			isInitiator = false;
			previousMatchEnd = tmpEnd;
		}
		//
		p = Pattern.compile("\\s+(\\S+(\\s\\S+)*)\\s+Trader\\sFeedback:");
		m.usePattern(p);
		if (m.find(previousMatchEnd))
		{
			author = m.group(1);
			previousMatchEnd = m.end(1);
		}
		else
		{
			throw new NoSuchFieldException("cannot find author in " + metaDataLine);
		}
	}
	
	private void parseTextLine() throws NoSuchFieldException
	{
		for (String previousAuthor : threadAuthors)
		{
			Pattern p = Pattern.compile("Quote:\\s+Originally Posted by " + Pattern.quote(previousAuthor) + "\\s+");
			Matcher m = p.matcher(textLine);
		
			if (m.find())
			{
				replyTo = previousAuthor;
				break;
			}
		}
	}
	
	public static void main(String[] args)
	{
		Scanner sc = ReadWrite.fileScanner(args[0]);
		BufferedWriter bw = ReadWrite.outputWriter(args[1]);
		String threadInitiator = null;
		ArrayList<String> threadAuthors = new ArrayList<String>();
		String name;
		String messageIdLine = sc.nextLine();
		String titleLine = sc.nextLine();
		String metaDataLine = sc.nextLine();
		String textLine; // = sc.nextLine();
		boolean messageCorrect = false;
		
		while (sc.hasNextLine())
		{
			textLine = sc.nextLine();
			try
			{
				try
				{
					messageCorrect = messageIdLine.substring(0,10).equals("MessageId:") &&
									titleLine.substring(0, 6).equals("Title:") &&
									metaDataLine.substring(0, 9).equals("MetaData:") &&
									textLine.substring(0, 5).equals("Text:");
					
					if (messageCorrect)
					{				
						SingleMessage message = new SingleMessage(messageIdLine, titleLine, 
								metaDataLine, textLine, threadInitiator, threadAuthors);
						if (message.isInitiator) threadInitiator = message.author;
						if (message.messageId == 1) 
							while (!threadAuthors.isEmpty())
								threadAuthors.remove(threadAuthors.size()-1);
						threadAuthors.add(message.author);
						
						try
						{
							if (message.replyTo == null) name = "-1&*";
							else name = message.replyTo;
							bw.write(message.threadId + " " + message.messageId + " " + 
									message.author + " " + message.date + " " + message.time + " "
									+ name + "\n");
						} 
						catch (IOException ie)
						{
							System.out.println("error -- " + ie.toString());
						}
					}
				}
				catch (StringIndexOutOfBoundsException siobe)
				{
					//System.out.println ("error -- " + siobe.toString());
				}
			}
			catch (NoSuchFieldException nfe)
			{
				System.out.println("error -- " + nfe.toString());
				continue;
			}
			finally
			{
				messageIdLine = titleLine;
				titleLine = metaDataLine;
				metaDataLine = textLine;
			}
		}
		
		try
		{
			bw.close();
		} 
		catch (IOException ie)
		{
			System.out.println("error -- " + ie.toString());
		}
	}
}