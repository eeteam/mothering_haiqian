package mothering.statistics;

class Edge implements Comparable<Edge>
{
	int fromId, toId;
	
	Edge(int fromId, int toId)
	{
		this.fromId = fromId;
		this.toId = toId;
	}
	
	@Override
	public boolean equals(Object other)
	{
		Edge tmp = (Edge) other;
		if (fromId == tmp.fromId && toId == tmp.toId)
			return true;
		else
			return false;
	}
	
	@Override
	public int hashCode()
	{
		return 137 * fromId + 3 * toId;
	}
	
	@Override
	public int compareTo(Edge other)
	{
		if (fromId < other.fromId) {
			return -1;
		} else if (fromId > other.fromId) {
			return 1;
		} else {
			if (toId < other.toId)
				return -1;
			else if (toId > other.toId)
				return 1;
			else
				return 0;
		}
			
	}
}