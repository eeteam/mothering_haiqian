
import java.io.*;

public class TestLine
{
    public static void main(String[] args)
    {
	//Scanner sc = fileScanner(args[0]);
	try
	{
	    FileReader fr = new FileReader(args[0]);
	    BufferedReader br = new BufferedReader(fr);

	    String line;
	    while ((line = br.readLine()) != null) {
		System.out.println(line);
	    }
	}
	catch (Exception e)
	{
	    System.out.println("error -- " + e.toString());
	}
    }
}