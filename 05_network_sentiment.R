library(igraph)

dat <- scan("edge_list_new1.txt", what=list(0,0,0));

edge.list <- cbind(dat[[1]], dat[[2]]);

g <- graph.edgelist(edge.list);

E(g)$weight <- dat[[3]];

names <- scan("sorted_user_id_new.txt", what="a", sep="\n");
for (i in 1:length(names))
{
    names[i] <- strsplit(names[i], "%")[[1]][1];
}

## ---------------------------

threshold <- -0.5;
dg <- delete.edges(g, E(g)[E(g)$weight > threshold]);
ecount(dg)

thresholds <- seq(0.1, 10, 0.1);
ens <- array(0, dim=c(length(thresholds),2));
for (i in 1:length(thresholds))
{
    ens[i,1] <- thresholds[i];
    dg <- delete.edges(g, E(g)[E(g)$weight < thresholds[i]]);
    ens[i,2] <- ecount(dg);
}

## ---------------------------
gs <- simplify(dg)

dg.o <- degree(gs, mode="out");
names[order(dg.o, decreasing=TRUE)[1:20]]

cc <- decompose.graph(gs)

cc.size <- rep(0, length(cc));
for (i in 1:length(cc))
{
    cc.size[i] <- vcount(cc[[i]]);
}

plot(cc.size)

gcc <- cc[[1]];

gcc <- set.graph.attribute(gcc, "layout", layout.fruchterman.reingold(gcc, weight=E(gcc)$weight));

jpeg("network_new_sentiment.jpg", width=2000, height=2000)
par(mar=c(1,1,1,1))
plot(gcc, vertex.label="", vertex.size=1.4, edge.arrow.size=0.7, vertex.color='firebrick4')
dev.off()

## ------------------------------------------
tg <- as.undirected(gcc, mode="collapse");

el <- get.edgelist(gcc);
E(tg)$weight <- NA;
for (i in 1:dim(el)[1])
{
    from <- el[i,1];
    to <- el[i,2];
    if (is.na(E(tg,path=c(from,to))$weight))
    {
        E(tg,path=c(from,to))$weight <- E(gcc,path=c(from,to))$weight;
    }
    else
    {
        E(tg,path=c(from,to))$weight <- E(tg,path=c(from,to))$weight + E(gcc,path=c(from,to))$weight;
    }
}

save(tg, file="weighted_undirected_gcc_new_sentiment.rda")

fg1 <- fastgreedy.community(tg, weight=-E(tg)$weight)
save(fg1, file="gcc_undirected_weight_fg_new_sentiment.rda")

cm <- community.to.membership(tg, fg1$merges, steps=which.max(fg1$modularity)-1);
modularity(tg, cm$membership, weights=E(tg)$weight)

## -------------------------------------------------

cl <- clusters(gs)
gcc.idx <- (0:(vcount(gs)-1))[cl$membership == 0];

group <- 0;
members <- gcc.idx[cm$membership == group];
del.v <- V(tg)[cm$membership != group];
cg <- delete.vertices(tg, del.v)
dg.o <- degree(cg);
members[order(dg.o, decreasing=TRUE)[1:5]]
names[members[order(dg.o, decreasing=TRUE)[1:10]] + 1]


